<?php

namespace Rainy\Api;

use Illuminate\Support\ServiceProvider;

class RainyApiServiceServiceProvider extends ServiceProvider
{

    // Services
    protected $services = [
        'Authentication',
        'Tickets',
        'User',
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Boot routes
        foreach ($this->services as $service) {
            include __DIR__ . "/Services/{$service}/routes.php";
        }

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        foreach ($this->services as $service) {
            $this->app->make("Rainy\Api\Services\{$service}\Controllers\MainController");
        }

    }
}
