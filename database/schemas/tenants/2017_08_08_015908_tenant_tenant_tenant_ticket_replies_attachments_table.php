<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Orchestra\Tenanti\Migration;

class TenantTenantTenantTicketRepliesAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @param  string|int  $id
     * @param  \Illuminate\Database\Eloquent\Model  $model
     *
     * @return void
     */
    public function up($id, Model $model)
    {
        Schema::create("tenant_{$id}_ticket_reply_attachments", function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticket_reply_id');
            $table->string('uri');
            $table->string('body')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @param  string|int  $id
     * @param  \Illuminate\Database\Eloquent\Model  $model
     *
     * @return void
     */
    public function down($id, Model $model)
    {
        Schema::drop("tenant_{$id}_ticket_reply_attachments");
    }
}
