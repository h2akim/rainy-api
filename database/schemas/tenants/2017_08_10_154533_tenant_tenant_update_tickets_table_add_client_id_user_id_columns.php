<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Orchestra\Tenanti\Migration;

class TenantTenantUpdateTicketsTableAddClientIdUserIdColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @param  string|int  $id
     * @param  \Illuminate\Database\Eloquent\Model  $model
     *
     * @return void
     */
    public function up($id, Model $model)
    {
        Schema::table("tenant_{$id}_tickets", function (Blueprint $table) {
            $table->dropColumn('owner');
        });

        Schema::table("tenant_{$id}_tickets", function (Blueprint $table) {
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('client_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @param  string|int  $id
     * @param  \Illuminate\Database\Eloquent\Model  $model
     *
     * @return void
     */
    public function down($id, Model $model)
    {
        Schema::table("tenant_{$id}_tickets", function (Blueprint $table) {
            $table->integer('owner');
        });

        Schema::table("tenant_{$id}_tickets", function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('client_id');
        });
    }
}
