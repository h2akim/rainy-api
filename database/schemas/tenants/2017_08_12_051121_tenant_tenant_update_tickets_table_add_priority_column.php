<?php

use Orchestra\Tenanti\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;

class TenantTenantUpdateTicketsTableAddPriorityColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @param  string|int  $id
     * @param  \Illuminate\Database\Eloquent\Model  $model
     *
     * @return void
     */
    public function up($id, Model $model)
    {
        Schema::table("tenant_{$id}_tickets", function (Blueprint $table) {
            $table->integer('priority')->unsigned()->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @param  string|int  $id
     * @param  \Illuminate\Database\Eloquent\Model  $model
     *
     * @return void
     */
    public function down($id, Model $model)
    {
        Schema::table("tenant_{$id}_tickets", function (Blueprint $table) {
            $table->dropColumn('priority');
        });
    }
}
