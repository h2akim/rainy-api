<?php

use Illuminate\Database\Seeder;

class PrioritiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('priorities')->insert([
            'id' => 1,
            'key' => 'low',
            'value' => 'Low'
        ]);

        DB::table('priorities')->insert([
            'id' => 2,
            'key' => 'Medium',
            'value' => 'medium'
        ]);

        DB::table('priorities')->insert([
            'id' => 3,
            'key' => 'High',
            'value' => 'high'
        ]);
    }
}
