<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class StatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
            'key' => 'open',
            'value' => 'OPEN'
        ]);

        DB::table('statuses')->insert([
            'key' => 'closed',
            'value' => 'CLOSE'
        ]);
    }
}
