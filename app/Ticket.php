<?php

namespace App;

use App\Traits\TableNameSpoofer;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use TableNameSpoofer;

    protected $hidden = ['user_id', 'client_id', 'status_id', 'pivot'];

    public function replies()
    {
        return $this->hasMany(TicketReply::class)->orderBy('created_at', 'asc');
    }

    public function assignees()
    {
        return $this->belongsToMany(User::class, $this->getTablePrefix() . 'ticket_assignees', 'ticket_id', 'user_id')->withTimestamps();
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, $this->getTablePrefix() . 'ticket_tags', 'ticket_id', 'tag_id')->where('active', true)->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function repliesCount()
    {
        return $this->hasMany(TicketReply::class)->selectRaw('ticket_id, count(*) as total')->groupBy('ticket_id');
    }

    public function assignedToCurrentUser()
    {
        return $this->hasMany(TicketAssignee::class)->where('user_id', \App\User::currentUser()->id);
    }
}
