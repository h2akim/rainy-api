<?php

class RestResponse
{
    public $items = [];
    public $status = 200;

    public function __construct($items, $status)
    {
        $this->items = $items;
        $this->status = is_null($status) ? 200 : $status;
    }
}

function restResponse($items, $status = null)
{
    $response = new \RestResponse($items, $status);
    return $response;
}
