<?php

namespace App\Extensions;

use Illuminate\Cache\DatabaseStore;
use Illuminate\Contracts\Cache\Store;

class MultiDatabase extends DatabaseStore implements Store
{

    protected $table;
    protected $prefix;
    protected $encrypter;
    protected $connection;

    public function __construct(ConnectionInterface $connection, EncrypterContract $encrypter,
        $table, $prefix = '') {
        $this->table = $table;
        $this->prefix = config('cache.multidatabase.prefix');
        $this->encrypter = $encrypter;
        $this->connection = $connection;
    }

}
