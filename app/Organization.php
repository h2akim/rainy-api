<?php

namespace App;

use App\Client;
use App\Traits\TableNameSpoofer;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    use TableNameSpoofer;

    public function clients()
    {
        return $this->hasMany(Client::class);
    }
}
