<?php

namespace App\Observers;

use Orchestra\Tenanti\Observer;

class TenantObserver extends Observer
{
	public function getDriverName()
	{
		return 'tenant';
	}
}
