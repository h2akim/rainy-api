<?php

namespace App;

use App\Traits\TableNameSpoofer;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    protected $fillable = ['setting', 'value'];

    use TableNameSpoofer;

    protected $hidden = ['editable'];
}
