<?php

namespace App\Traits;

trait TableNameSpoofer
{
    /*
    Spoof default getTable function with tenant id from current request
    @return string
     */
    public function getTable()
    {
        return app()->make('TenantService')->tablePrefix() . parent::getTable();
    }

    public function getTablePrefix()
    {
        return app()->make('TenantService')->tablePrefix();
    }

}
