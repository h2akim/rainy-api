<?php

namespace App;

use App\Traits\TableNameSpoofer;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use TableNameSpoofer;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function replies()
    {
        return $this->belongsTo(TicketReply::class);
    }
}
