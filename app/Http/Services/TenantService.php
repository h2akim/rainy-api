<?php

namespace App\Http\Services;

use Illuminate\Support\Facades\Cache;

class TenantService
{

    protected $tenant;

    public function __construct()
    {

        $prefix = 'tenant_';
        $tenant_id = config('tenantId');

        if (Cache::has($prefix . $tenant_id)) {
            $tenant = cache($prefix . $tenant_id);
        } else {
            $tenant = \App\Tenant::where('id', $tenant_id)->first();
        }

        $this->tenant = $tenant;
    }

    public function tablePrefix()
    {
        return 'tenant_' . $this->tenant->id . '_';
    }

    public function getTenantId()
    {
        return $this->tenant->id;
    }

    public function getTenantName()
    {
        return $this->tenant->subdomain;
    }

    public function getSubdomain()
    {
        return $this->tenant->subdomain;
    }

    public function getDomain()
    {
        return $this->tenant->domain;
    }
}
