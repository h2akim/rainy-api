<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingController extends Controller
{

    public function getSettings()
    {

        try {

            $settings = \App\Setting::get();
            $fullSettings = [];
            foreach ($settings as $setting) {
                $fullSettings[$setting->setting] = $setting->value;
            }

            $response = restResponse(['settings' => $fullSettings], 200);

        } catch (\Exception $e) {

            $response = restResponse($e, 500);

        }

        return response()->json($response->items, $response->status);

    }

    public function putUpdateSetting(Request $request)
    {

        $inputs = $request->params;

        $allowed_settings = ['org_name'];

        \DB::beginTransaction();

        try {

            foreach ($inputs as $key => $value) {
                if (in_array($key, $allowed_settings)) {
                    $setting = \App\Setting::firstOrCreate([
                        'setting' => $key]);
                    if (!$setting->editable) {
                        continue;
                    }

                    $setting->value = $value;
                    $setting->save();
                }
            }

            $response = restResponse(['success' => true], 200);

        } catch (\Exception $e) {

            \DB::rollBack();
            $response = restResponse($e, 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);
    }
}
