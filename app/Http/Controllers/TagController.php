<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TagController extends Controller
{

    public function getTags()
    {

        try {
            $tags = \App\Tag::active()->get();
            $response = restResponse(['tags' => $tags], 200);
        } catch (\Exception $e) {
            $response = restResponse($e, 500);
        }
        return response()->json($response->items, $response->status);

    }

    public function addTag(Request $request)
    {
        \DB::beginTransaction();
        try {

            $tag = new \App\Tag;
            $tag->body = $request->body;
            $tag->tag_group_id = $request->groupId;
            $tag->text_color = $request->text_color;
            $tag->background_color = $request->background_color;
            $tag->active = true;
            $tag->save();
            $response = restResponse(['tag' => $tag], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse($e, 500);
        }
        \DB::commit();
        return response()->json($response->items, $response->status);
    }

    public function fetchTags($id)
    {

        $tags = \App\TagGroup::with('tags')->find($id);
        return response()->json($tags);
    }

    public function addLabelGroup(Request $request)
    {

        \DB::beginTransaction();

        try {

            $labelGroup = new \App\TagGroup;
            $labelGroup->group_name = $request->group_name;
            $labelGroup->save();

            $response = restResponse(['group' => $labelGroup], 200);

        } catch (\Exception $e) {

            \DB::rollBack();
            $response = restResponse($e, 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);

    }

    public function fetchTagGroups($type = null)
    {

        try {
            $tags = \App\TagGroup::with(['tags' => function ($q) use ($type) {
                if ($type) {
                    $type = ($type == 'active') ? true : false;
                    $q->where('active', $type);
                }
            }])->get();
            $response = restResponse(['groups' => $tags], 200);
        } catch (\Exception $e) {
            $response = restResponse($e, 500);
        }
        return response()->json($response->items, $response->status);

    }

    public function deleteTagGroup($id)
    {

        \DB::beginTransaction();

        try {

            $group = \App\TagGroup::find($id);
            $ids = $group->tags->pluck('id');

            $tags = \App\Tag::whereIn('id', $ids)->delete();
            $group->delete();

            $response = restResponse(['success' => true], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse($e, 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);

    }

    public function deleteTag($id)
    {

        \DB::beginTransaction();

        try {

            $tags = \App\Tag::find($id)->delete();
            $tickets = \App\TicketTag::where('tag_id', $id)->delete();
            $response = restResponse(['success' => true], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse($e, 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);

    }

    public function toggleTag(Request $request)
    {

        \DB::beginTransaction();

        try {

            $tag = \App\Tag::find($request->id);
            $tag->active = !$tag->active;
            $tag->save();

            $response = restResponse(['tag' => $tag], 200);

        } catch (\Exception $e) {

            \DB::rollBack();
            $response = restResponse($e, 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);

    }

}
