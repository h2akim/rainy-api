<?php

namespace App\Http\Controllers;

use App\Tenant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class ResolverController extends Controller
{
    // Method: POST
    public function validateRoute(Request $request)
    {
        // get subdomain
        $sub_domain = $request->sub_domain;

        if (!$sub_domain) {
            return response()->json(['response_code' => 401, 'success' => false], 401);
        }

        // check for cache
        if (Cache::has($sub_domain)) {
            return response()->json(['response_code' => 200, 'success' => true]);
        } else if ($tenant = Tenant::where('subdomain', $sub_domain)
            ->orWhere('domain', $sub_domain)->first()) {
            return response()->json(['response_code' => 200, 'success' => true, 'tenantId' => $tenant->id, 'tenantName' => $tenant->subdomain]);
        } else {
            return response()->json(['status' => 404, 'success' => false], 404);
        }

    }

    // Method: GET
    public function getTenant(Request $request)
    {
        // get sub-domain
        $sub_domain = $request->sub_domain;

        // check for cache
        if (Cache::has($sub_domain)) {
            return response()->json([Cache::get($sub_domain)]);
        } else if ($tenant = Tenant::where('subdomain', $sub_domain)
            ->orWhere('domain', $sub_domain)->first()) {
            return response()->json(['tenant_id' => $tenant->id]);
        } else {
            return response()->json(['status' => 404, 'success' => false], 404);
        }
    }
}
