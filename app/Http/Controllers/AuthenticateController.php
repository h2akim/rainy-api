<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;

class AuthenticateController extends Controller
{
    /**
     * Authenticate User
     *
     * @return json
     */
    public function authenticate(Request $request)
    {

        // grab credentials from the request
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            //  Check does tenant id and tenancy name is valid or not
            try {
                $subdomain_tenant = \App\Tenant::where('id', $request->header('tenantId'))->where('subdomain', $request->header('tenancyName'))->
                    orWhere('domain', $request->header('tenancyName'))->firstOrFail();
            } catch (\Exception $e) {
                return response()->json(['error' => 'could_not_find_tenant'], 500);
            }

            if (!$token = JWTAuth::attempt($credentials,
                [
                    'tenant_id' => $request->header('tenantId'),
                    'tenant_name' => $request->header('tenancyName'),
                ])) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }

        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = [
            'name' => JWTAuth::toUser($token)->name,
            'id' => JWTAuth::toUser($token)->id,
        ];

        $exp = JWTAuth::getPayLoad()->get('exp');

        // all good so return the tokene
        return response()->json(compact(['token', 'user', 'exp']));

    }

    /**
     * summary
     *
     * @return void
     * @author
     */
    public function logout(Request $request)
    {
        try {
            JWTAuth::parseToken()->invalidate();
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e], 200);
        }

        return response()->json(['success' => true], 200);

    }
}
