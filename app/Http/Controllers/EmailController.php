<?php

namespace App\Http\Controllers;

class EmailController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = \App\User::currentUser();
    }

    public function sendEmail()
    {
        //
    }

    public function retrieveEmail()
    {
        //
    }

    public function processEmail()
    {
        // Process Email from Sparkpost webhook
    }

}
