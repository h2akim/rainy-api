<?php

namespace App\Http\Controllers;

use App\Jobs\SendTicketReply;
use App\Mailbox;
use App\Ticket;
use App\TicketReply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MailboxController extends Controller
{
    public function __construct()
    {
        // resolve which tenant first
    }

    public function receiveMail(Request $request)
    {

        // 85a4ff76b61f4994ff49cd46b063024362a7c3ac@chitcat.xyz
        // https://c6063440.ngrok.io/api/v1/mailbox

        Log::info($request);
        $newEmail = false;

        $mailbox = \App\Mailbox::where('address', $request->recipient)->first();
        if ($mailbox) {
            config(['tenantId' => $mailbox->tenant_id]);
        } else {
            return response()->json(['success' => false]);
        }

        preg_match("/(.*) <(.*)>/", $request->from, $from);
        $sender_email = $from[2];
        $sender_name = trim($from[1], '"');

        // Search for client or agents
        $client = \App\Client::where('email', $sender_email)->first();
        $user = \App\User::where('email', $sender_email)->first();

        $person = null;

        if ($client) {
            $person = $client;
        } else if ($user) {
            $person = $user;
        }

        // If record not exists treat as client
        if (!$person) {

            $person = new \App\Client;
            $person->email = !is_null($sender_email) ? $sender_email : '';
            $person->name = !is_null($sender_name) ? $sender_name : 'Unknown sender';
            $person->save();

        }

        // find ticket signature belongsTo
        $signature = isset($request->{'In-Reply-To'}) ? trim($request->{'In-Reply-To'}, '<>') : trim($request->{'Message-Id'}, '<>');
        $ticket = \App\Ticket::where('signature', $signature)->first();

        if (!$ticket) {

            // New Ticket

            $ticket = new \App\Ticket;
            $ticket->subject = $request->{'subject'};
            $ticket->status_id = 1;
            $ticket->priority = 1;
            $ticket->channel = 1;
            $ticket->signature = trim($request->{'Message-Id'}, '<>');

            $newEmail = true;

        }

        $_reply = new \App\TicketReply;
        $_reply->internal = 0;
        $_reply->body = $request->{'stripped-html'};
        if ($user) {
            $_reply->user_id = $person->id;
            $ticket->user_id = $person->id;
        } else {
            $_reply->client_id = $person->id;
            $ticket->client_id = $person->id;
        }

        $ticket->save();
        $ticket->replies()->save($_reply);

        if ($newEmail) {
            // dispatch email to acknowledge user
            $data = [
                'email' => \App\Mailbox::where('tenant_id', config('tenantId'))->first()->address,
                'organization' => \App\Setting::where('setting', 'org_name')->first()->value,
                'subject' => $ticket->subject,
                'title' => 'thank you for your email.',
                'body' => 'Your email is important to us, we will respond to your email shortly.',
                'footer' => 'To add more information, just reply to this email. &#128521; &#8210; Attachment is not yet supported &#128557;',
            ];

            SendTicketReply::dispatch($ticket, $_reply, $data);
        }

    }

}
