<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ClientController extends Controller
{

    public function getClients()
    {
        try {
            $clients = \App\Client::all();
            $response = restResponse(['clients' => $clients], 200);
        } catch (\Exception $e) {
            $response = restResponse(['error' => 'Oops error during fetching clients'], 500);
        }
        return response()->json($response->items, $response->status);
    }

    public function getClient($id)
    {
        try {
            $client = \App\Client::find($id);
            $response = restResponse(['client' => $client], 200);
        } catch (\Exception $e) {
            $response = restResponse(['error' => 'Oops error during fetching client'], 500);
        }
        return response()->json($response->items, $response->status);
    }

    public function postClient(Request $request)
    {
        \DB::beginTransaction();
        try {

            if (\App\Client::where('email', $request->email)->first()) {
                throw new \Exception;
            }

            $client = new \App\Client;
            $client->email = $request->email;
            $client->name = $request->name;
            $client->save();

            $response = restResponse(['client' => $client], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse(['error' => 'Oops error during adding new client'], 500);
        }

        \DB::commit();
        return response()->json($response->items, $response->status);
    }

    public function toggleClient(Request $request)
    {

        \DB::beginTransaction();

        try {

            $client = \App\Client::find($request->id);
            $client->active = !$client->active;
            $client->save();

            $response = restResponse(['client' => $client], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse($e, 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);

    }

    public function deleteClient($id)
    {
        \DB::beginTransaction();

        try {

            $client = \App\Client::find($id)->delete();
            $tickets = \App\Ticket::where('client_id', $id)->delete();
            $reply = \App\Ticket::where('client_id', $id)->delete();

            $response = restResponse(['success' => true], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse($e, 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);
    }

    public function putClient()
    {
        try {

        } catch (\Exception $e) {

        }
    }

}
