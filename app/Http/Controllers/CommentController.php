<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function postComment(Request $request)
    {

        \DB::beginTransaction();

        try {
            $ticket_id = $request->id;
            $comment_body = $request->body;

            $comment = new \App\Comment;
            $comment->ticket_reply_id = $ticket_id;
            $comment->user_id = \App\User::currentUser()->id;
            $comment->body = $comment_body;
            $comment->save();

            $response = restResponse(['comment' => $comment], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse($e, 500);
        }

        \DB::commit();
        return response()->json($response->items, $response->status);

        return response()->json($comment);
    }

    public function deleteComment()
    {

    }

    public function putComment()
    {

    }
}
