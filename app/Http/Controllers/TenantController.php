<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TenantController extends Controller
{

    public function getInfo(Request $request) {

        try {

            $tenantId = \App\Tenant::where('subdomain', app('request')->header('tenant'))->first()->id;
            config(['tenantId' => $tenantId]);
            $setting = \App\Setting::where('setting', 'org_name')->first();

            if ($setting) {
                return response()->json(['org_name' => $setting->value]);
            }

        } catch (\Exception $e) {

            return response()->json(['org_name' => 'Meeeeoooowww']);

        }


    }

}
