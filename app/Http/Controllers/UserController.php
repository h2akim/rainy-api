<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Get Current Authenticated User Information
     *
     * @return json
     */
    public function getCurrentUserDetails(Request $request)
    {
        return response()->json();
    }

    public function getAgents()
    {
        try {
            $agents = \App\User::all();
            $response = restResponse(['agents' => $agents], 200);
        } catch (\Exception $e) {
            $response = restResponse([], 500);
        }

        return response()->json($response->items, $response->status);
    }

    public function getAgent($id)
    {
        try {
            $agent = \App\User::find($id);
            $response = restResponse(['agent' => $agent], 200);
        } catch (\Exception $e) {
            $response = restResponse([], 500);
        }

        return response()->json($response->items, $response->status);

    }

    public function postAddNewUser(Request $request)
    {

        \DB::beginTransaction();

        try {

            $admin = \App\User::currentUser()->admin;
            if (!$admin) {
                throw new \Exception;
            }

            $user = new \App\User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->tenant_id = \App\User::currentUser()->tenant_id;
            $user->save();

            $response = restResponse(['user' => $user], 200);

        } catch (\Exception $e) {

            \DB::rollBack();
            $response = restResponse([], 500);

        }

        \DB::commit();

        return response()->json($response->items, $response->status);

    }

    public function putUser($id, Request $request)
    {

        \DB::beginTransaction();
        try {

            $admin = \App\User::currentUser()->admin;
            if (!$admin) {
                throw new \Exception;
            }

            $user = \App\User::find($id);
            $user->name = $request->name;
            $user->email = $request->email;

            if ($request->password) {
                $user->password = Hash::make($request->password);
            }

            $user->save();
            $response = restResponse(['user' => $user], 200);

        } catch (\Exception $e) {

            \DB::rollBack();
            $response = restResponse([], 500);

        }

        \DB::commit();

        return response()->json($response->items, $response->status);

    }
}
