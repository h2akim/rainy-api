<?php

namespace App\Http\Controllers;

use App\Jobs\SendTicketReply;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Get List of Ticket
     *
     * @return json
     */
    public function getTickets($type)
    {
        try {
            // open, closed, assigned, all
            switch ($type) {
                case 'open':
                case 'closed':

                    $tickets = \App\Ticket::where('status_id', \App\Status::where('key', $type)->first()->id)->with(['user', 'client', 'repliesCount', 'assignedToCurrentUser', 'status', 'tags'])->withCount('assignees')->orderBy('updated_at', 'DESC')->get();
                    break;

                case 'assigned':
                    $ticketList = \App\TicketAssignee::where('user_id', \App\User::currentUser()->id)->pluck('ticket_id');
                    $tickets = \App\Ticket::where('status_id', \App\Status::where('key', 'open')->first()->id)
                        ->whereIn('id', $ticketList)
                        ->with(['user', 'client', 'repliesCount', 'assignedToCurrentUser', 'status', 'tags'])
                        ->withCount('assignees')->orderBy('updated_at', 'DESC')->get();
                    break;

                case 'all':
                default:
                    $tickets = \App\Ticket::with(['user', 'client', 'repliesCount', 'assignedToCurrentUser', 'status', 'tags.tag'])->withCount('assignees')->orderBy('updated_at', 'DESC')->get();
            }
            $response = restResponse(['items' => $tickets]);
        } catch (\Exception $e) {
            $response = restResponse([], 500);
        }

        return response()->json($response->items, $response->status);
    }

    /**
     * Get ticket
     *
     * @return void
     * @author
     **/
    public function getTicket($type, $id)
    {

        try {
            $ticket = \App\Ticket::with('user', 'client')->where('id', $id);

            switch ($type) {
                case 'details':
                    $ticket = $ticket->first();
                    break;

                case 'replies':
                    $ticket = $ticket->with('replies.user', 'replies.client', 'replies.comments.user')->first();
                    break;

                case 'assignees':
                    $ticket = $ticket->with('assignees.user')->first();
                    break;

                case 'full':
                    $ticket = $ticket->with('replies.user', 'replies.client', 'assignees', 'tags', 'status', 'replies.comments.user')->first();
                    break;

                default:
                    throw new \Exception;
            }

            $response = restResponse(['items' => $ticket]);
        } catch (\Exception $e) {
            $response = restResponse([], 500);
        }

        return response()->json($response->items, $response->status);
    }

    /**
     * Add ticket
     *
     * @return json
     */
    public function postAddTicket(Request $request)
    {

        \DB::beginTransaction();

        try {

            // Saving ticket
            $ticket = new \App\Ticket;
            $ticket->channel = 1;
            $ticket->subject = $request->subject;
            $ticket->status_id = \App\Status::where('key', $request->status)->first()->id;
            $ticket->signature = hash('haval128,4', json_encode([
                'ticket_id' => $ticket->id,
                'user_id' => \App\User::currentUser()->id,
            ]));

            // Saving replies
            $reply = new \App\TicketReply;
            $reply->internal = false;
            $reply->body = $request->body;

            if ($request->client) {
                $ticket->client_id = \App\Client::find($request->client['id'])->id;
                $reply->client_id = \App\Client::find($request->client['id'])->id;
            } else {
                $ticket->user_id = \App\User::currentUser()->id;
                $reply->user_id = \App\User::currentUser()->id;
            }

            $ticket->save();
            $ticket->replies()->save($reply);

            // loop for assignees
            $nAssignee = array_map(function ($assignee) {
                return $assignee['id'];
            }, $request->assignees);
            $ticket->assignees()->attach($nAssignee);

            // loop for tags
            $nTag = array_map(function ($tag) {
                return $tag['id'];
            }, $request->tags);
            $ticket->tags()->attach($nTag);

            $response = restResponse(['ticket' => $ticket], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse($e, 500);
        }
        \DB::commit();
        return response()->json($response->items, $response->status);

    }

    /**
     * Update ticket
     *
     * @return json
     * @author
     */
    public function putTicket(Request $request, $id)
    {

        \DB::beginTransaction();

        try {

            $ticket = \App\Ticket::find($id);
            // status, priority, assignees, tags
            // $ticket->priority = $request->priority;
            // return response()->json($request->status);
            $status = \App\Status::select('id')->where('key', $request->status)->first();

            $ticket->status_id = $status->id;

            // tags

            $tags = array_map(function ($tag) {
                return $tag['id'];
            }, $request->tags);

            $ticket->tags()->sync($tags);

            // assignees
            $assignees = array_map(function ($assignee) {
                return $assignee['id'];
            }, $request->assignees);

            $ticket->assignees()->sync($assignees);

            $ticket->save();

        } catch (\Exception $e) {
            \DB::rollBack();
            return response()->json($e);
        }

        \DB::commit();
        return response()->json(['yay' => $request->all()], 200);
    }

    /**
     * add reply
     *
     * add reply to ticket
     *
     * @param $request
     * @return json
     */
    public function postAddReply(Request $request)
    {

        \DB::beginTransaction();

        try {
            $ticket = \App\Ticket::find($request->input('id'));

            $reply = new \App\TicketReply;
            $reply->user_id = \App\User::currentUser()->id;
            $reply->body = $request->input('body');
            $reply->internal = ($request->input('internal')) ? true : false;
            $ticket->replies()->save($reply);

            // organization name, actual email
            $extra = [
                'organization' => \App\Setting::where('setting', 'org_name')->first()->value,
            ];

            $data = [
                'email' => \App\Mailbox::where('tenant_id', config('tenantId'))->first()->address,
                'organization' => $extra['organization'],
                'title' => $reply->user->name . ' has reply to your ticket [#' . $ticket->id . ']',
                'body' => $reply->body,
                'subject' => $ticket->subject,
                'footer' => 'Kindly reply to this email to answer the ticket reply. &#128521;',
            ];

            SendTicketReply::dispatch($ticket, $reply, $data);

            $response = restResponse(['reply' => $reply], 200);

        } catch (\Exception $e) {
            \DB::rollBack();
            $response = restResponse($e, 500);
        }

        \DB::commit();
        return response()->json($response->items, $response->status);
    }

}
