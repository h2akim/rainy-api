<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class AccountController extends Controller
{

    public function register(Request $request)
    {

        \DB::beginTransaction();

        try {
            // register new tenant
            $tenant = new \App\Tenant;
            $tenant->subdomain = $request->subdomain . '.' . env('APP_DOMAIN');
            $tenant->domain = $request->subdomain . '.' . env('APP_DOMAIN');
            $tenant->save();

            //  register mailbox
            $mailbox = new \App\Mailbox;
            $mailbox->address = hash('ripemd160', uniqid()) . '@' . env('APP_DOMAIN');
            $mailbox->tenant_id = $tenant->id;
            $mailbox->save();

            // Create related table
            \Artisan::call('tenanti:migrate', [
                'driver' => 'tenant', '--id' => $tenant->id
            ]);

            config(['tenantId' => $tenant->id]);
            //  add admin user
            $user = new \App\User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = \Hash::make($request->password);
            $user->tenant_id = $tenant->id;
            $user->admin = true;
            $user->save();

            $response = restResponse(['success', true], 200);

        } catch (\Exception $e) {

            // rollback new table created for current new tenant
            \Artisan::call('tenanti:rollback', [
                'driver' => 'tenant', '--id' => config('tenantId')
            ]);

            // rollback all add to tenant table and mailbox table
            \DB::rollBack();

            // drop migration table for deleted table
            Schema::drop('tenant_' . config('tenantId') . '_migrations');

            $response = restResponse($e, 400);

        }

        //  seed most of the things
        \DB::commit();

        return response()->json($response->items, $response->status);


    }

    public function getProfile()
    {
        try {

            $user = \App\User::currentUser()->with('profile')->first();
            $response = restResponse(['user' => $user]);

        } catch (\Exception $e) {
            $response = restResponse([], 500);
        }

        return response()->json($response->items, $response->status);
    }

    public function putProfile(Request $request)
    {

        \DB::beginTransaction();

        try {

            $user = \App\User::currentUser()->first();
            $profile = \App\Profile::where('user_id', \App\User::currentUser())->first();
            if ($profile) {
                $user->name = $request->name;
                $user->email = $request->email;
                $profile->signature = $request->profile['signature'];
                $profile->timezone = ($request->profile['timezone']) ? $request->profile['timezone'] : "UTC";
            } else {
                $user->name = $request->name;
                $user->email = $request->email;

                $profile = new \App\Profile;
                $profile->user_id = \App\User::currentUser()->id;
                $profile->signature = $request->profile['signature'];
                $profile->timezone = ($request->profile['timezone']) ? $request->profile['timezone'] : "UTC";
            }

            $profile->save();
            $user->save();

            $response = restResponse(['success' => true], 200);

        } catch (\Exception $e) {

            \DB::rollBack();
            $response = restResponse(['success' => false], 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);

    }

    public function putPassword(Request $request)
    {

        \DB::beginTransaction();

        try {
            $user = \App\User::currentUser()->first();
            if (\Auth::attempt(['email' => $user->email, 'password' => $request->current])) {
                $user->password = \Hash::make($request->new);
                $user->save();

            } else {
                throw new \Exception;
            }

            $response = restResponse(['success' => true], 200);

        } catch (\Exception $e) {

            \DB::rollBack();
            $response = restResponse(['success' => false], 500);

        }

        \DB::commit();
        return response()->json($response->items, $response->status);

    }

    public function postRegister(Request $request)
    {
        //
    }

}
