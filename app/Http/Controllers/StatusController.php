<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function getStatuses() {
        try {
            $statuses = \App\Status::all();
            $response = restResponse([ 'statuses' => $statuses ], 200);
        } catch (\Exception $e) {
            $response = restResponse([], 500);
        }

        return response()->json($response->items, $response->status);

    }
}
