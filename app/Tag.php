<?php

namespace App;

use App\Traits\TableNameSpoofer;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use TableNameSpoofer;

    protected $hidden = ['tag_group_id', 'created_at', 'updated_at'];

    public function scopeActive()
    {
        return $this->where('active', true);
    }

    public function group()
    {
        return $this->belongsTo(\App\TagGroup::class);
    }
}
