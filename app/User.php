<?php

namespace App;

use App\Ticket;
use App\TicketAssignee;
use App\Traits\TableNameSpoofer;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use JWTAuth;

class User extends Authenticatable
{
    use Notifiable;
    use TableNameSpoofer;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'tenant_user_id', 'tenant_id', 'created_at', 'updated_at', 'pivot',
    ];

    public function tenant()
    {
        return $this->belongsTo('App\Tenant');
    }

    public static function currentUser()
    {
        return JWTAuth::toUser(JWTAuth::getToken());
    }

    public function tickets()
    {
        return $this->belongsToMany(Ticket::class);
    }

    public function assignedTickets()
    {
        return $this->hasMany(TicketAssignee::class);
    }

    public function scopeActive()
    {
        return $this->where('active', true);
    }

    public function profile()
    {
        return $this->hasOne(\App\Profile::class);
    }

}
