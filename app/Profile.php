<?php

namespace App;

use App\Traits\TableNameSpoofer;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use TableNameSpoofer;
}
