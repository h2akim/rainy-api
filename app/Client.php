<?php

namespace App;

use App\Organization;
use App\Traits\TableNameSpoofer;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use TableNameSpoofer;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'created_at', 'updated_at',
    ];

    public function organization()
    {
        return $this->hasMany(Organization::class);
    }
}
