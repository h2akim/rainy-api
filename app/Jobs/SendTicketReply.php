<?php

namespace App\Jobs;

use App\Ticket;
use App\TicketReply;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendTicketReply implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $ticket;
    private $reply;
    private $data;
    private $replace;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket, TicketReply $reply, $data, $replace = [])
    {
        $this->ticket = $ticket;
        $this->reply = $reply;
        $this->data = $data;
        $this->replace = $replace;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        // try {

        $reply_email = !is_null($this->ticket->user_id) ? $this->ticket->user->email : $this->ticket->client->email;
        $reply_name = !is_null($this->ticket->user_id) ? $this->ticket->user->name : $this->ticket->client->name;

        \Mailgun::send('emails.transaction', $this->data, function ($message) use ($reply_email, $reply_name) {
            $message
                ->header('Message-Id', $this->ticket->signature)
                ->header('In-Reply-To', $this->ticket->signature)
                ->from($this->data['email'], $this->data['organization'])
                ->to($reply_email, $reply_name)
                ->subject($this->data['subject'] . ' [#' . $this->ticket->id . ']');
        });

        // } catch (\Exception $e) {

        // }

        // Log::info('sending reply' . json_encode($this->ticket) . ' - ' . json_encode($this->reply));
    }
}
