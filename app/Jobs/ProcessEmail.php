<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $request;
    private $tenant;

    /**
    * Create a new job instance.
    *
    * @return void
    */
    public function __construct($request, $tenant)
    {
        $this->request = $request;
        $this->tenant = $tenant;
    }

    /**
    * Execute the job.
    *
    * @return void
    */
    public function handle()
    {
        try {

            $ticket = new \App\Ticket;
            $ticket->subject = $this->request->{'subject'};
            $ticket->status_id = 1;
            $ticket->priority = 1;
            $ticket->client_id = 1;

            // Search for client or agents
            $client = \App\Client::where('email', $this->request->sender)->first();
            $user = User::where('email', $this->request->sender)->first();

            $person = null;

            if ($client) {
                $person = $client;
            } else if ($user) {
                $person = $user;
            }

            // If record not exists treat as client
            if (!$person) {

                $person = new \App\Client;
                $person->email = $this->request->sender;
                $person->name = str_replace('<'.$this->request->sender.'>', '', $this->request->from);
                $person->save();

            }

            $ticket->signature = hash('haval128,4', json_encode([
                'ticket_id' => $ticket->id,
                'person_id' => $person->id,
            ]));

            $_reply = new \App\TicketReply;
            $_reply->internal = 0;
            $_reply->body = $this->request->{'stripped-html'};
            if ($user) {
                $_reply->user_id = $person->id;
            } else {
                $_reply->client_id = $person->id;
            }

            $ticket->save();
            $ticket->replies()->save($_reply);

        } catch (\Exception $e) {

            return response()->json(false, 500);

        }

        return response()->json(true, 200);
    }
}
