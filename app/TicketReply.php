<?php

namespace App;

use App\Client;
use App\Ticket;
use App\Traits\TableNameSpoofer;
use App\User;
use Illuminate\Database\Eloquent\Model;

class TicketReply extends Model
{
    use TableNameSpoofer;

    protected $hidden = ['ticket_id'];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function client()
    {
        return $this->belongsTo(Client::class, 'client_id');
    }

    public function count()
    {
        return $this->count();
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
