<?php

namespace App;

use App\Tag;
use App\Traits\TableNameSpoofer;
use Illuminate\Database\Eloquent\Model;

class TagGroup extends Model
{
    use TableNameSpoofer;

    protected $hidden = ['created_at', 'updated_at'];

    public function tags()
    {
        return $this->hasMany(Tag::class);
    }

    public function tagsActive()
    {
        return $this->hasMany(Tag::class)->where('active', true);
    }
}
