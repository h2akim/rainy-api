<?php

namespace App;

use App\Ticket;
use App\Traits\TableNameSpoofer;
use App\User;
use Illuminate\Database\Eloquent\Model;

class TicketAssignee extends Model
{

    protected $hidden = ['ticket_id', 'user_id', 'id'];
    protected $fillable = ['user_id'];

    use TableNameSpoofer;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tickets()
    {
        return $this->belongsTo(Ticket::class);
    }

}
