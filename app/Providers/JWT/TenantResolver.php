<?php

namespace App\Providers\JWT;

use Exception;
use Namshi\JOSE\JWS;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Providers\JWT\NamshiAdapter;

class TenantResolver extends NamshiAdapter
{
    /**
     * Decode a JSON Web Token.
     *
     * @param  string  $token
     * @return array
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     */
    public function decode($token)
    {
        try {
            $jws = JWS::load($token);
        } catch (Exception $e) {
            throw new TokenInvalidException('Could not decode token: ' . $e->getMessage());
        }
        try {
            $subdomain_tenant = $jws->getPayload()['tenant_id'] != app()->make('TenantService')->getTenantId();
        } catch (\Exception $e) {
            throw new JWTException('Invalid Tenant', 500);
        }

        if (!$jws->verify($this->secret, $this->algo) ||
            $subdomain_tenant) {
            throw new TokenInvalidException('Token Signature could not be verified.');
        }

        return $jws->getPayload();
    }
}
