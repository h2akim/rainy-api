<?php

namespace App\Providers;

use App\Http\Services\TenantService;
use App\Observers\TenantObserver;
use App\Tenant;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Tenant::observe(new TenantObserver);

        $tenantId = $this->app->request->header('tenantId');
        if ($tenantId) {
            config(['tenantId' => $tenantId]);
        }

        $tenant_id = config('tenantId');
        $prefix = 'tenant_';

        if (Cache::has($prefix . $tenant_id)) {
            $tenant = cache($prefix . $tenant_id);
        } else {
            $tenant = \App\Tenant::where('id', $tenant_id)->first();
            $expiresAt = Carbon::now()->addMinutes(15);
            cache([$prefix . $tenant_id => $tenant], $expiresAt);
        }
        $this->app->bind('TenantService', function ($app) use ($tenant) {
            return new TenantService;
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('mailgun.client', function () {
            return \Http\Adapter\Guzzle6\Client::createWithConfig([]);
        });
    }
}
