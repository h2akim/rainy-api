<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    public function getRouteKeyName()
    {
        return 'subdomain';
    }

    public function mailboxes() {
        return $this->hasMany(\App\Mailbox::class);
    }
}
