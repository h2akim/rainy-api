<?php

namespace App;

use App\Traits\TableNameSpoofer;
use Illuminate\Database\Eloquent\Model;

class TicketTag extends Model
{
    use TableNameSpoofer;

    protected $fillable = ['tag_id'];

    public function tickets()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }
}
