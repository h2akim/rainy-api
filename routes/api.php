<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')
    ->group(function () {

        // Validate route
        Route::post('/validate-route', 'ResolverController@validateRoute');

        // Get current Tenant Id
        Route::get('/get-tenant', 'ResolverController@getTenant');

        // Register
        Route::post('/register', 'AccountController@register');

        Route::post('/mailbox', 'MailboxController@receiveMail');

        // Authentication Service
        Route::group(['prefix' => 'Authentication'], function () {

            // Authenticate User
            Route::post('/authenticate', 'AuthenticateController@authenticate');

            // Logout
            Route::post('/logout', 'AuthenticateController@logout');

        });

        Route::group(['prefix' => 'TenantServices'], function () {

            Route::get('/getInfo', 'TenantController@getInfo');

        });

        // Route Need Authentication
        Route::group(['middleware' => ['jwt.auth']], function () {

            // User Services
            Route::group(['prefix' => 'UserServices'], function () {
                // Get User Information
                Route::get('/getCurrentUserDetails', 'UserController@getCurrentUserDetails');

                // Get Agent List
                Route::get('/getAgents', 'UserController@getAgents');

                // Get Agent List
                Route::get('/getAgent/{id}', 'UserController@getAgent');

                // Put Agent
                Route::put('/putUser/{id}', 'UserController@putUser');

                // Post Agent
                Route::post('/addUser', 'UserController@postAddNewUser');
            });

            // Ticket Services
            Route::group(['prefix' => 'TicketServices'], function () {
                // Get Tickets
                Route::get('/getTickets/{type}', 'TicketController@getTickets');

                // Get Ticket
                Route::get('/getTicket/{type}/{id}', 'TicketController@getTicket');

                // Post Add Ticket
                Route::post('/postAddTicket', 'TicketController@postAddTicket');

                // Put Update Ticket
                Route::put('/putTicket/{id}', 'TicketController@putTicket');

                // Post Add Reply
                Route::post('/postReply', 'TicketController@postAddReply');

                // Comments
                Route::group(['prefix' => 'Comments'], function () {

                    // Post Comment
                    Route::post('/postComment', 'CommentController@postComment');

                });
            });

            // Tagging Services
            Route::group(['prefix' => 'TagServices'], function () {
                // Get Tag List
                Route::get('/getTags', 'TagController@getTags');

                // Get Tags from tag group
                Route::get('/fetchTags/{id}', 'TagController@fetchTags');

                // Get Tag Groups
                Route::get('/fetchTagGroups/{type?}', 'TagController@fetchTagGroups');

                // add Tag
                Route::post('/addTag', 'TagController@addTag');

                // delete tag
                Route::delete('/deleteLabel/{id}', 'TagController@deleteTag');

                // toggle Tag (active/deative)
                Route::post('/toggleTag', 'TagController@toggleTag');

                // add Group Label
                Route::post('/addLabelGroup', 'TagController@addLabelGroup');

                // delete group label
                Route::delete('/deleteLabelGroup/{id}', 'TagController@deleteTagGroup');
            });

            // Tagging Services
            Route::group(['prefix' => 'StatusServices'], function () {
                // Get Tag List
                Route::get('/getStatuses', 'StatusController@getStatuses');
            });

            // Client Services
            Route::group(['prefix' => 'ClientServices'], function () {
                // Get Client List
                Route::get('/getClients', 'ClientController@getClients');

                // Get Client
                Route::get('/getClient/{id}', 'ClientController@getClient');

                // add Client
                Route::post('/postClient', 'ClientController@postClient');

                // toggle Client
                Route::post('/toggleClient', 'ClientController@toggleClient');

                // delete Client
                Route::delete('/deleteClient/{id}', 'ClientController@deleteClient');
            });

            // Setting Services
            Route::group(['prefix' => 'SettingServices'], function () {
                // Get Settings
                Route::get('/getSettings', 'SettingController@getSettings');

                // put Setting
                Route::put('/putSettings', 'SettingController@putUpdateSetting');
            });

            // Account Services
            Route::group(['prefix' => 'AccountServices'], function () {
                // Get Settings
                Route::get('/getProfile', 'AccountController@getProfile');

                // put Setting
                Route::put('/putProfile', 'AccountController@putProfile');

                // put Password
                Route::put('/putPassword', 'AccountController@putPassword');
            });

        });
    });
