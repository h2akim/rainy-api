<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::domain('{tenant}.api.dev')
        ->middleware('cors')
        ->group(function ($tenant) {
            Route::get('/tohok', function ($tenant) {
                return response()->json($tenant);
            });
        });*/

Route::get('/', function () {
    return view('welcome');
});
